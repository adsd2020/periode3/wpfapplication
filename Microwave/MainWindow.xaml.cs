﻿namespace Microwave
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;
    using Models;
    using ViewModels;

    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly Microwave _microwave;

        /// <summary>
        ///     constructor of the wpf window
        /// </summary>
        public MainWindow()
        {
            _microwave = new Microwave();
            InitializeComponent();

            // set the data context of the window to can use it for all data bindings
            DataContext = _microwave;

            // add each food in the food collection of the microwave attribute into the window.
            foreach (var food in _microwave.Foods)
            {
                // make the picture
                var foodImage = new Image
                {
                    Width = 200,
                    Height = 200,
                    Name = food.Name,
                    Source = new BitmapImage(new Uri(food.PicturePath, UriKind.Relative))
                };

                // Bind the visibility of each specific added food picture to IsVisible property of it's object
                BindingOperations.SetBinding(foodImage, VisibilityProperty,
                    new Binding
                    {
                        Source = food, Path = new PropertyPath("IsVisible"),
                        Converter = new BooleanToVisibilityConverter()
                    });

                // add the picture to food canvas and set it's position
                FoodCanvas.Children.Add(foodImage);
                Canvas.SetTop(foodImage, 220);
                Canvas.SetLeft(foodImage, 310);
            }

            // disable the food combobox and reset button.
            ComboBoxFoods.IsEnabled = false;
            FoodReset.IsEnabled = false;
        }

        /// <summary>
        ///     On mousedown event of the custom windows header witch we made self, to make the window draggable.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormHeader_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed) DragMove();
        }

        /// <summary>
        ///     Behavior of the Exit button, when is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Exit_OnClick(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        /// <summary>
        ///     Behavior of the Start button, when is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Start_OnClick(object sender, RoutedEventArgs e)
        {
            _microwave.TurnOn();
        }

        /// <summary>
        ///     Behavior of the Stop button, when is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Stop_OnClick(object sender, RoutedEventArgs e)
        {
            _microwave.TurnOff();
        }

        /// <summary>
        ///     Behavior of the door button, when is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Door_OnClick(object sender, RoutedEventArgs e)
        {
            switch (_microwave.Door.State)
            {
                case DoorState.Closed:
                    Door.Content = "Close the door";
                    _microwave.Door.Open();
                    Start.IsEnabled = false;
                    ComboBoxFoods.IsEnabled = true;
                    FoodReset.IsEnabled = true;

                    // put the food canvas behind the microwave picture with closed door
                    Panel.SetZIndex(FoodCanvas, 2);
                    break;
                case DoorState.Open:
                    Door.Content = "Open the door";
                    _microwave.Door.Close();
                    Start.IsEnabled = true;
                    ComboBoxFoods.IsEnabled = false;
                    FoodReset.IsEnabled = false;

                    // put the food canvas front the microwave picture with open door
                    Panel.SetZIndex(FoodCanvas, 0);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        ///     Behavior of the time down button, when is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimerDownTen_OnClick(object sender, RoutedEventArgs e)
        {
            _microwave.TimeIncrease(-10);
        }

        /// <summary>
        ///     Behavior of the time up button, when is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimerUpTen_OnClick(object sender, RoutedEventArgs e)
        {
            _microwave.TimeIncrease(10);
        }

        /// <summary>
        ///     Behavior of the food combobox, when de selected item changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ComboBoxFoods.SelectedIndex > -1)
                for (var i = 0; i < ComboBoxFoods.Items.Count; i++)
                    if (i == ComboBoxFoods.SelectedIndex)
                        _microwave.Foods[i].Show();
                    else
                        _microwave.Foods[i].Hide();
        }

        /// <summary>
        ///     Behavior of the Reset button, when is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FoodReset_OnClick(object sender, RoutedEventArgs e)
        {
            for (var i = 0; i < ComboBoxFoods.Items.Count; i++)
                _microwave.Foods[i].Hide();

            ComboBoxFoods.SelectedIndex = -1;
        }

        /// <summary>
        ///     Event on media ended to make the loop of the engine on video
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MediaElement_OnMediaEnded(object sender, RoutedEventArgs e)
        {
            EngineOnPicture.Position = TimeSpan.Zero;
            EngineOnPicture.Play();
        }

        /// <summary>
        ///     Event to play or stop the engine video then it's visibility changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EngineOnPicture_OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (EngineOnPicture.Visibility == Visibility.Visible)
            {
                EngineOnPicture.Position = TimeSpan.Zero;
                EngineOnPicture.Play();
            }
            else
            {
                EngineOnPicture.Stop();
            }
        }
    }
}