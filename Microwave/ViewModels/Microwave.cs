﻿namespace Microwave.ViewModels
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Media;
    using Models;
    using Models.Foods;
    using Properties;

    /// <summary>
    ///     A object to compose whole microwave with all of it's parts
    ///     <inheritdoc cref="INotifyPropertyChanged" />
    /// </summary>
    public class Microwave : INotifyPropertyChanged
    {
        private Door _door = new Door();
        private Engine _engine = new Engine();
        private Lamp _lamp = new Lamp();

        // a general state of the defined enum type below, used to show and hide the microwave pictures
        private MicrowavePictureState _pictureState;
        private StopTimer _stopTimer = new StopTimer();

        // A string to demonstrate the seconds as a string in the timer panel of the microwave
        private string _timeString = "00:00";

        /// <summary>
        ///     Constructor to set the beginning things
        /// </summary>
        public Microwave()
        {
            // set the behavior of property changed of the timer, what would happened if the timer notified a change in it's properties.
            StopTimer.PropertyChanged += delegate(object sender, PropertyChangedEventArgs e)
            {
                if (PropertyChanged == null) return;
                if (e.PropertyName == nameof(StopTimer.State) && StopTimer.State == TimerState.Off)
                {
                    Lamp.TurnOff();
                    Engine.TurnOff();
                }
                else if (e.PropertyName == nameof(StopTimer.Second))
                {
                    var timerSecond = StopTimer.Second;
                    TimeString = $"{timerSecond / 60:D2} : {timerSecond % 60:D2}";
                }
            };

            // set the behavior of property changed of the door, what would happened if the door notified a change in it's properties.
            Door.PropertyChanged += (sender, args) =>
            {
                if (PropertyChanged == null) return;
                SoundPlayer doorSound;

                if (Door.State != DoorState.Closed)
                {
                    TurnOff();
                    doorSound = new SoundPlayer(Resources.microwave_open);
                    doorSound.Play();
                    PictureState = MicrowavePictureState.DoorOpen;
                }
                else
                {
                    PictureState = Engine.State == EngineState.Off
                        ? MicrowavePictureState.DoorClosed
                        : MicrowavePictureState.EngineOn;
                    doorSound = new SoundPlayer(Resources.microwave_close);
                    doorSound.Play();
                }
            };

            // set the behavior of property changed of the engine, what would happened if the engine notified a change in it's properties.
            Engine.PropertyChanged += (sender, args) =>
            {
                if (PropertyChanged == null) return;
                var engineSound = new SoundPlayer(Resources.EngineSound);

                if (Engine.State == EngineState.On)
                {
                    PictureState = MicrowavePictureState.EngineOn;
                    engineSound.PlayLooping();
                }
                else
                {
                    engineSound.Stop();
                    PictureState = Door.State == DoorState.Closed
                        ? MicrowavePictureState.DoorClosed
                        : MicrowavePictureState.DoorOpen;
                }
            };

            // set the beginning state of all part of the microwave.
            Door.Close();
            Lamp.TurnOff();
            Engine.TurnOff();
            StopTimer.Stop();
            StopTimer.Reset();

            // Add the made instance of the foods to the observable collection below
            Foods.Add(new Chicken());
            Foods.Add(new Coffee());
            Foods.Add(new Pizza());
            Foods.Add(new Poffertjes());
            Foods.Add(new Salmon());
        }

        /// <summary>
        ///     Property to achieve door attribute
        ///     set the property changed to notify the wpf form
        /// </summary>
        public Door Door
        {
            get => _door;
            set
            {
                _door = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Door)));
            }
        }

        /// <summary>
        ///     Property to achieve lamp attribute
        ///     set the property changed to notify the wpf form
        /// </summary>
        public Lamp Lamp
        {
            get => _lamp;
            set
            {
                _lamp = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Lamp)));
            }
        }

        /// <summary>
        ///     Property to achieve engine attribute
        ///     set the property changed to notify the wpf form
        /// </summary>
        public Engine Engine
        {
            get => _engine;
            set
            {
                _engine = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Engine)));
            }
        }

        /// <summary>
        ///     Property to achieve timer attribute
        ///     set the property changed to notify the wpf form
        /// </summary>
        public StopTimer StopTimer
        {
            get => _stopTimer;
            set
            {
                _stopTimer = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(StopTimer)));
            }
        }


        /// <summary>
        ///     Property to achieve time string attribute, which will shown in the timer panel
        ///     set the property changed to notify the wpf form
        /// </summary>
        public string TimeString
        {
            get => _timeString;
            set
            {
                _timeString = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TimeString)));
            }
        }

        /// <summary>
        ///     Property to achieve picture state attribute
        ///     set the property changed to notify the wpf form
        /// </summary>
        public MicrowavePictureState PictureState
        {
            get => _pictureState;
            set
            {
                _pictureState = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(PictureState)));
            }
        }

        /// <summary>
        ///     The collection of the foods, which can be added to the microwave.
        ///     it is a observable collection to notify every changes
        /// </summary>
        public ObservableCollection<IFood> Foods { get; set; } = new ObservableCollection<IFood>();

        // the event handler of the property changed, the member of INotifyPropertyChanged interface
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///     Turned the engine on and if the timer is zero it will be set to 30 seconds
        /// </summary>
        public void TurnOn()
        {
            if (Door.State == DoorState.Closed)
            {
                if (StopTimer.Second == 0) StopTimer.Second = 30;

                StopTimer.Start();
                Lamp.TurnOn();
                Engine.TurnOn();
            }
        }

        /// <summary>
        ///     Turn the engine off.
        /// </summary>
        public void TurnOff()
        {
            StopTimer.Stop();
            Lamp.TurnOff();
            Engine.TurnOff();
        }

        /// <summary>
        ///     Increase the second property of the timer.
        /// </summary>
        /// <param name="second"></param>
        public void TimeIncrease(int second)
        {
            StopTimer.Second += second;
        }
    }

    /// <summary>
    ///     The enum variable to set the microwave picture state
    /// </summary>
    public enum MicrowavePictureState
    {
        DoorClosed,
        DoorOpen,
        EngineOn
    }
}