﻿namespace Microwave.BindingConverter
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;
    using ViewModels;

    /// <summary>
    /// Value converter for binding Picture state to the microwave pictures.
    /// </summary>
    public class PictureVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// Convert method of the converter, member of IValueConverter interface
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //control that this converter only used for PicturesState.
            if (!(value is MicrowavePictureState curentState)) return Visibility.Collapsed;

            //define a visibility variable to return
            var visibilityState = Visibility.Collapsed;

            //check which picture used this converter.
            switch (parameter)
            {
                //if the picture or video of open door used this
                case "OpenDoor":

                    //if Pictures state DoopOpen is, set the visibility variable to visible, otherwise to collapse
                    visibilityState = curentState == MicrowavePictureState.DoorOpen
                        ? Visibility.Visible
                        : Visibility.Collapsed;
                    break;
                
                //if the picture of closed door used this
                case "ClosedDoor":

                    //if Pictures state DoorClosed is, set the visibility variable to visible, otherwise to collapse
                    visibilityState = curentState == MicrowavePictureState.DoorClosed
                        ? Visibility.Visible
                        : Visibility.Collapsed;
                    break;

                //if the video of engine on used this
                case "EngineOn":

                    //if Pictures state EngineOn is, set the visibility variable to visible, otherwise to collapse
                    visibilityState = curentState == MicrowavePictureState.EngineOn
                        ? Visibility.Visible
                        : Visibility.Collapsed;
                    break;
            }

            //return the visibility variable
            return visibilityState;
        }

        /// <summary>
        /// ConvertBack method of the converter, member of IValueConverter interface.
        /// This method wil not be used, therefore not implemented.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}