﻿namespace Microwave.Models.Foods
{
    using System.ComponentModel;

    /// <summary>
    ///     <inheritdoc cref="IFood" />
    /// </summary>
    public class Poffertjes : IFood
    {
        //private boolean attribute to hold the sate of the food that is visible or not.
        private bool _isVisible;

        /// <summary>
        ///     Constructor of the class to set the default values
        ///     the food shall be at the beginning invisible.
        /// </summary>
        public Poffertjes()
        {
            IsVisible = false;
            PicturePath = "Assets/Pictures/Poffertjes.png";
        }

        //property change event handler, member of the INotifyChanges interface
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///     <inheritdoc cref="IFood.IsVisible" />
        /// </summary>
        public bool IsVisible
        {
            get => _isVisible;
            set
            {
                _isVisible = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsVisible)));
            }
        }

        /// <summary>
        ///     <inheritdoc cref="IFood.PicturePath" />
        /// </summary>
        public string PicturePath { get; }

        /// <summary>
        ///     <inheritdoc cref="IFood.Name" />
        /// </summary>
        public string Name { get; } = "Poffertjes";

        /// <summary>
        ///     <inheritdoc cref="IFood.Show" />
        /// </summary>
        public void Show()
        {
            IsVisible = true;
        }

        /// <summary>
        ///     <inheritdoc cref="IFood.Hide" />
        /// </summary>
        public void Hide()
        {
            IsVisible = false;
        }
    }
}