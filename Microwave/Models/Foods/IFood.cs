﻿namespace Microwave.Models.Foods
{
    using System.ComponentModel;

    /// <summary>
    ///     foods, which wil be added into microwave
    ///     <inheritdoc cref="INotifyPropertyChanged" />
    /// </summary>
    public interface IFood : INotifyPropertyChanged
    {
        /// <summary>
        ///     a boolean property ti save the visibility state
        /// </summary>
        bool IsVisible { get; }

        /// <summary>
        ///     property to save the path of the picture.
        /// </summary>
        string PicturePath { get; }

        /// <summary>
        ///     property to save the name of the food
        /// </summary>
        string Name { get; }

        /// <summary>
        ///     Make the picture of the food visible
        /// </summary>
        void Show();

        /// <summary>
        ///     Hide the picture of the food.
        /// </summary>
        void Hide();
    }
}