﻿namespace Microwave.Models
{
    using System.ComponentModel;

    /// <summary>
    ///     A enum for the state of microwave lamp
    /// </summary>
    public enum LampState
    {
        On,
        Off
    }

    /// <summary>
    ///     Microwave lamp, a part of microwave
    ///     <inheritdoc cref="INotifyPropertyChanged" />
    /// </summary>
    public class Lamp : INotifyPropertyChanged
    {
        //A private attribute to save the state of the lamp
        private LampState _state;

        /// <summary>
        ///     The property to achieve the state
        ///     set method will notify when attribute changed.
        /// </summary>
        public LampState State
        {
            get => _state;
            set
            {
                _state = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(State)));
            }
        }

        //property change event handler, member of the INotifyChanges interface
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///     Set the state of he lamp to ON
        /// </summary>
        public void TurnOn()
        {
            if (State != LampState.On)
                State = LampState.On;
        }

        /// <summary>
        ///     Set the state of he lamp to OFF
        /// </summary>
        public void TurnOff()
        {
            if (State != LampState.Off)
                State = LampState.Off;
        }
    }
}