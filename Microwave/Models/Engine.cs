﻿namespace Microwave.Models
{
    using System.ComponentModel;

    /// <summary>
    ///     A enum for the state of microwave engine
    /// </summary>
    public enum EngineState
    {
        On,
        Off
    }


    /// <summary>
    ///     Microwave engine, a part of microwave
    ///     <inheritdoc cref="INotifyPropertyChanged" />
    /// </summary>
    public class Engine : INotifyPropertyChanged
    {
        //A private attribute to save the state of the engine
        private EngineState _state;

        /// <summary>
        ///     The property to achieve the state
        ///     set method will notify when attribute changed.
        /// </summary>
        public EngineState State
        {
            get => _state;
            set
            {
                _state = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(State)));
            }
        }

        //property change event handler, member of the INotifyChanges interface
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///     Set the state of he engine to ON
        /// </summary>
        public void TurnOn()
        {
            if (State != EngineState.On)
                State = EngineState.On;
        }

        /// <summary>
        ///     Set the state of he engine to OFF
        /// </summary>
        public void TurnOff()
        {
            if (State != EngineState.Off)
                State = EngineState.Off;
        }
    }
}