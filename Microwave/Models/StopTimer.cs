﻿namespace Microwave.Models
{
    using System.ComponentModel;
    using System.Media;
    using System.Timers;
    using Properties;

    /// <summary>
    ///     A enum for the state of microwave timer
    /// </summary>
    public enum TimerState
    {
        On,
        Off
    }


    /// <summary>
    ///     Microwave timer, a part of microwave
    ///     <inheritdoc cref="INotifyPropertyChanged" />
    /// </summary>
    public class StopTimer : INotifyPropertyChanged
    {
        //A private readonly time to can use the Timer of the system library 
        private readonly Timer _timer;

        //A private attribute for the time which timer must shown.
        private long _second;

        //A private attribute to save the state of the engine
        private TimerState _state;

        /// <summary>
        ///     constructor to set the default values of the timer object
        /// </summary>
        public StopTimer()
        {
            //se the interval of the timer to one second
            _timer = new Timer(1000);

            // Hook up the Elapsed event for the timer. 
            _timer.Elapsed += TimerOnElapsed;
        }

        /// <summary>
        ///     Property to achieve _second private attribute
        ///     set method will notify when attribute changed.
        /// </summary>
        public long Second
        {
            get => _second;
            set
            {
                if (value < 0) return;
                _second = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Second)));
            }
        }

        /// <summary>
        ///     Property to achieve the timer state
        ///     set method will notify when attribute changed.
        /// </summary>
        public TimerState State
        {
            get => _state;
            set
            {
                _state = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(State)));
            }
        }

        //property change event handler, member of the INotifyChanges interface
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///     When timer elapsed event happened, countdown the second
        ///     until zero, then run the stop method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            Second--;
            if (Second == 0)
            {
                Stop();

                var soundPlayer = new SoundPlayer(Resources.TimerStoped);
                soundPlayer.Play();
            }
        }

        /// <summary>
        ///     Set the state to ON and start the timer
        ///     If second is zero by default set it to 30 seconds.
        /// </summary>
        public void Start()
        {
            if (State != TimerState.On)
            {
                if (Second == 0)
                    Second = 30;
                _timer.Start();
                State = TimerState.On;
            }
            else
            {
                Second += 30;
            }
        }

        /// <summary>
        ///     Stop the timer and set the state to Off
        /// </summary>
        public void Stop()
        {
            if (State != TimerState.Off)
            {
                _timer.Stop();
                State = TimerState.Off;
            }
            else
            {
                Reset();
            }
        }

        /// <summary>
        ///     Reset the seconds to zero.
        /// </summary>
        public void Reset()
        {
            Second = 0;
        }
    }
}