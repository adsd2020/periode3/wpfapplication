﻿namespace Microwave.Models
{
    using System.ComponentModel;

    /// <summary>
    ///     A enum for the state of microwave door
    /// </summary>
    public enum DoorState
    {
        Open,
        Closed
    }

    /// <summary>
    ///     Microwave door, a part of microwave
    ///     <inheritdoc cref="INotifyPropertyChanged" />
    /// </summary>
    public class Door : INotifyPropertyChanged
    {
        //A private attribute to save the state of the door
        private DoorState _state;

        /// <summary>
        ///     The property to achieve the state
        ///     set method will notify when attribute changed.
        /// </summary>
        public DoorState State
        {
            get => _state;
            set
            {
                _state = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(State)));
            }
        }

        //property change event handler, member of the INotifyChanges interface
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///     Set the state of he door to open
        /// </summary>
        public void Open()
        {
            if (State != DoorState.Open)
                State = DoorState.Open;
        }

        /// <summary>
        ///     Set the state of the door to closed.
        /// </summary>
        public void Close()
        {
            if (State != DoorState.Closed)
                State = DoorState.Closed;
        }
    }
}